use regex::Regex;

pub struct HangmanSession {
    phrase: String,
    wrong_count: u8,
    pub revealed_letters: Vec<char>,
    pub guessed_letters: Vec<char>
}

impl HangmanSession {

    const ALLOWED_GUESSES:u8 = 6;

    pub fn new(new_phrase: String) -> HangmanSession {
        HangmanSession {
            phrase: new_phrase.to_uppercase(),
            wrong_count: 0,
            revealed_letters: vec![],
            guessed_letters: vec![]
        }
    }

    pub fn get_phrase_progress(&self) -> String {
        
        let hidden_phrase = self.phrase.clone();
        let all_revealed_letters: String = self.revealed_letters.iter().clone().collect::<String>();
        let revealed_letters_regex_format_syntax: String;
        if all_revealed_letters.len() > 0 {
            revealed_letters_regex_format_syntax = format!("[^{}\\W]", all_revealed_letters);
        } else {
            revealed_letters_regex_format_syntax = String::from("[A-Z]");
        }
        
        let revealed_letters_regex = Regex::new(&revealed_letters_regex_format_syntax).unwrap();
        revealed_letters_regex.replace_all(&hidden_phrase, "_").to_string()
    }

    pub fn get_remaining_guesses(&self) -> u8 { 
        HangmanSession::ALLOWED_GUESSES - self.wrong_count 
    }

    pub fn is_game_over(&self) -> bool {
        HangmanSession::ALLOWED_GUESSES == self.wrong_count
    }

    pub fn is_complete(&self) -> bool {
        let guessed_so_far = self.get_phrase_progress();

        return !guessed_so_far.contains("_")
    }

    pub fn get_phrase(&self) -> String {
        self.phrase.to_string()
    }

    pub fn add_guess(&mut self, guess: char) {
        if !self.guessed_letters.contains(&guess) {
            if !self.phrase.contains(guess) {
                self.guessed_letters.push(guess);
                self.wrong_count += 1;
            } else {
                if !self.revealed_letters.contains(&guess) {
                    self.revealed_letters.push(guess);    
                }
            }
        }
    }

    pub fn get_wrong_count(&self) -> u8 {
        self.wrong_count
    }

}
