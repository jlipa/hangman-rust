mod hangman_session;

use hangman_session::HangmanSession;
use std::io::*;
use std::fs;
use std::path;
use rand::Rng;

struct HangmanGame { }

impl HangmanGame {
    const HEAD_APPEARS:u8 = 1;
    const BODY_APPEARS:u8 = 2;
    const LEFT_ARM_APPEARS:u8 = 3;
    const RIGHT_ARM_APPEARS:u8 = 4;
    const LEFT_LEG_APPEARS:u8 = 5;
    const RIGHT_LEG_APPEARS:u8 = 6;
}

fn main() {

    println!("Hangman!");

    // Load all phrases
    let all_phrases = get_all_phrases();

    // Choose phrase at random
    let random_phrase_index = rand::thread_rng().gen_range(0..all_phrases.len());
    let random_phrase: String = all_phrases[random_phrase_index].to_string();

    let mut session:HangmanSession = HangmanSession::new(random_phrase);

    // Draw diagram
    draw_diagram(&session);

    // Play
    loop {
        // Take input
        println!("Next guess (input a single letter): ");
        let mut next_guess:String = String::new();
        stdin()
            .read_line(&mut next_guess)
            .expect("Unable to read take input");

        // Verify input is letter
        next_guess = next_guess.to_uppercase();
        let guess_ascii_value:u8 = next_guess.as_bytes()[0]; // Only get first item/letter in array
        
        if guess_ascii_value > 90 || guess_ascii_value < 65 {
            println!("Please enter a letter.");
            continue;
        } else {
            session.add_guess(char::from(guess_ascii_value));
        }

        draw_diagram(&session);

        // Reveal puzzle with new letter/new wrong count
        if session.is_game_over() {
            println!();
            println!("GAME OVER! -- Exhausted guesses!");
            println!("Solution: {}", session.get_phrase());
            break;
        }

        if session.is_complete() {
            println!();
            println!("Done! -- GREAT JOB!");
            println!("Solution: {}", session.get_phrase());
            break;
        }
    }   
}

fn get_all_phrases() -> Vec<String> {
    // https://riptutorial.com/rust/example/4275/read-a-file-line-by-line
    
    let phrase_file_path = path::Path::new("phrases.txt");
    let mut all_phrases: Vec<String> = vec![];
    let phrase_file = fs::File::open(&phrase_file_path).unwrap();
    let phrase_reader = BufReader::new(phrase_file);

    // Read the file line by line using the lines() iterator from std::io::BufRead.
    for (_, line) in phrase_reader.lines().enumerate() {
        let phrase = line.unwrap(); // Ignore errors.
        all_phrases.push(phrase);
    }
    all_phrases
}

pub fn draw_diagram(current_session:&HangmanSession) {
    
    println!("   -----------");
    println!("   |         |");
    println!("   |        {}", get_head(&current_session) );
    println!("   |        {}{}{}", get_left_arm(&current_session), get_body(&current_session), get_right_arm(&current_session));
    println!("   |        {} {}", get_left_leg(&current_session), get_right_leg(&current_session));
    println!("   |");
    println!("   |");
    println!("   |");
    println!("___|___");
    println!();

    println!("Phrase: {}", current_session.get_phrase_progress());
    println!("Guesses remaining: {}", current_session.get_remaining_guesses());
    println!("Incorrect Letters Guessed: {:?}", current_session.guessed_letters);
    println!("Correct Letters Guessed: {:?}", &current_session.revealed_letters);
}

fn get_head(current_session:&HangmanSession) -> String {
    if current_session.get_wrong_count() >= HangmanGame::HEAD_APPEARS { String::from(" O ") } else { String::from("") }
}

fn get_body(current_session:&HangmanSession) -> String {
    if current_session.get_wrong_count() >= HangmanGame::BODY_APPEARS { String::from("|") } else { String::from("") }
}

fn get_right_arm(current_session:&HangmanSession) -> String {
    if current_session.get_wrong_count() >= HangmanGame::RIGHT_ARM_APPEARS { String::from("\\") } else { String::from("") }
}

fn get_left_arm(current_session:&HangmanSession) -> String {
    if current_session.get_wrong_count() >= HangmanGame::LEFT_ARM_APPEARS { String::from("/") } else { String::from(" ") }
}

fn get_right_leg(current_session:&HangmanSession) -> String {
    if current_session.get_wrong_count() >= HangmanGame::RIGHT_LEG_APPEARS { String::from("\\") } else { String::from("") }
}

fn get_left_leg(current_session:&HangmanSession) -> String {
    if current_session.get_wrong_count() >= HangmanGame::LEFT_LEG_APPEARS { String::from("/") } else { String::from("") }
}